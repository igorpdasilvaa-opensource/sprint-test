package com.example.springbootdocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class SpringBootDockerApplication {

	@RequestMapping("/")
	String home (){
		return "Java é baum de mais !!!";
	}

	@RequestMapping("/verdade")
	String ForaDaHome (){
		return "... socorro";
	}

	@RequestMapping("/verdade-verdadeira")
	String ForaDaHome2 (){
		return "Vou brincar com e sse spring-boot";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDockerApplication.class, args);
	}

}
